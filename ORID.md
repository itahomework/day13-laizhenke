## Daily Report(07/26)

- O: learned React Router, Promise and Ant Design.
- R: benefit a lot.
- I: React Router allows you to define different routes in a single page application and render corresponding components based on URL changes. It is similar to Vue Router and the configuration method is also basically the same. By using Promise, asynchronous code can be handled more elegantly, avoiding the problem of callback hells. I didn't know much about Promise before, only the simplest usage, such as then, resolve.Ant Design is a React based component library developed by Alibaba. I have previously used ElementUI, which is based on Vue. For me, using ElementUI is more convenient, and I think the development documentation for ElementUI is better than Ant Design. To become proficient in using AntDesign, I need more practice.
- D: learned responsive layout, and do more practice for Ant Design.