import { NavLink, Outlet } from 'react-router-dom';
import { Menu, Col, Row } from 'antd';
import './App.css';

function App() {
  return (

    <div className="app">
      <Menu mode="horizontal" style={{ width: '250px' }}>
        <Menu.Item key="home">
          <NavLink to={'/'}>Home</NavLink>
        </Menu.Item>
        <Menu.Item key="done">
          <NavLink to={'/done'}>Done List</NavLink>
        </Menu.Item>
        <Menu.Item key="help">
          <NavLink to={'/help'}>Help</NavLink>
        </Menu.Item>
      </Menu>

      <Row justify="center" align="middle">
        <Col xs={8} sm={12} md={16} lg={24}>
          <Outlet />
        </Col>
      </Row>
    </div>
  );
}
export default App;
