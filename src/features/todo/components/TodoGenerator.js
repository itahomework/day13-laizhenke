import { useState } from "react";
import { useTodos } from '../../hooks/useTodo.js'
import { Input, Button } from 'antd'

export default function TodoGenerator() {
  const [taskName, setTaskName] = useState("");
  const { addTodo } = useTodos()

  const handleTaskNameChange = (e) => {
    const value = e.target.value;
    setTaskName(value);
  }

  const handleAddTodoTask = async () => {
    addTodo({ name: taskName })
    setTaskName("");
  }

  return (
    <div className='todo-generator'>
      <Input placeholder="input a new todo here..." onChange={handleTaskNameChange} value={taskName}></Input>
      <Button type="primary" onClick={handleAddTodoTask} disabled={!taskName}>Add</Button>
    </div>
  );
}