import DoneItem from "./DoneItem";
import { useSelector } from 'react-redux'
import { List } from 'antd';

export default function TodoGroup() {
  const todoTasks = useSelector(state => state.todo.tasks).filter(todo => todo.done === true)
  return (
    <List
      grid={{ gutter: 16, column: 1 }}
      dataSource={todoTasks}
      renderItem={(todoTask) => <List.Item>
        <DoneItem key={todoTask.id} task={todoTask}></DoneItem>
      </List.Item>}
    />
  );
}