import TodoItem from "./TodoItem";
import { useSelector } from 'react-redux'
import { useEffect } from "react";
import { useTodos } from '../../hooks/useTodo.js'
import { List } from 'antd';

export default function TodoGroup() {
  const todoTasks = useSelector(state => state.todo.tasks)
  const { loadTodos } = useTodos()

  useEffect(() => {
    loadTodos()
    // eslint-disable-next-line
  }, [])

  return (
    <List
      grid={{ gutter: 0, column: 1 }}
      dataSource={todoTasks}
      renderItem={(todoTask) => <List.Item>
        <TodoItem key={todoTask.id} task={todoTask}></TodoItem>
      </List.Item>}
    />
  );
}