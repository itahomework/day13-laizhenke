import { useDispatch } from "react-redux";
import { removeTodoTask } from "../reducers/todoSlice";
import { useNavigate } from "react-router-dom";
import { Button } from 'antd';
import { CloseOutlined } from '@ant-design/icons';

export default function TodoItem(props) {
  const dispatch = useDispatch();
  const navigation = useNavigate()

  const handleTaskNameClick = () => {
    navigation('/done/' + props.task.id)
  }

  const hanlleRemoveButtonClick = () => {
    if (window.confirm('Are you sure you wish to delete this item?')) {
      dispatch(removeTodoTask(props.task.id));
    }
  }
  return (
    <div className='todo-item'>
      <div className={`task-name ${props.task.done ? 'done' : ''} task`} onClick={handleTaskNameClick}>
        {props.task.name}
      </div>
      <Button shape="circle" icon={<CloseOutlined />} onClick={hanlleRemoveButtonClick} />
    </div >
  );
}