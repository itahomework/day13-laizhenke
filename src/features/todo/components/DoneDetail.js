import { useParams } from "react-router-dom"
import { useSelector } from "react-redux/es/hooks/useSelector"

export default function DoneDetail() {
  const { id } = useParams()
  const todoTask = useSelector(state => state.todo.tasks).find(task => task.id === id)
  return (
    <div className="todo-detail">
      <h1>Done Detail</h1>
      <div>id:{todoTask?.id}</div>
      <div>name:{todoTask?.name}</div>
    </div>
  )
}