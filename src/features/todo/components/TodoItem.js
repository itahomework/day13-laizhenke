import { useTodos } from '../../hooks/useTodo.js'
import { Button, Input, Modal } from 'antd';
import { CloseOutlined, EditOutlined, SearchOutlined } from '@ant-design/icons';
import { useState } from 'react';

export default function TodoItem({ task }) {
  const { updateTodo, deleteTodo } = useTodos()
  const [newTodoName, setNewTodoName] = useState(task.name);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);

  const handleTaskNameClick = async () => {
    await updateTodo(task.id, { done: !task.done })
  }

  const handleRemoveButtonClick = async () => {
    if (window.confirm('Are you sure you wish to delete this item?')) {
      await deleteTodo(task.id)
    }
  }

  const showModal = () => {
    setNewTodoName(task.name);
    setIsUpdateModalOpen(true);
  };

  const handleOk = async () => {
    await updateTodo(task.id, { name: newTodoName })
    setIsUpdateModalOpen(false);
  };

  const handleCancel = () => {
    setIsUpdateModalOpen(false);
    setNewTodoName('');
  };

  const changeNewTodoName = (e) => {
    setNewTodoName(e.target.value)
  }

  const todoDetail = () => {
    Modal.info({
      title: 'This is task detail',
      content: (
        <div>
          <p>{task.name}</p>
        </div>
      ),
      onOk() { },
    });
  }

  return (
    <div className='todo-item'>
      <div className={`task-name ${task.done ? 'done' : ''} task`} onClick={handleTaskNameClick}>
        {task.name}
      </div>
      <div className="btn-group">
        <Button className='btn' shape="circle" icon={<SearchOutlined />} onClick={todoDetail} />
        <Button className='btn' shape="circle" icon={<CloseOutlined />} onClick={handleRemoveButtonClick} />
        <Button className='btn' shape="circle" icon={<EditOutlined />} onClick={showModal} />
      </div>
      <Modal
        title="Edit Message"
        open={isUpdateModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Input value={newTodoName} onChange={changeNewTodoName} ></Input>
      </Modal>
    </div>
  );
}