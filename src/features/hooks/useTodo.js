import { useDispatch } from "react-redux"
import { deleteTodoTask, getTodoTasks, updateTodoTask, addTodoTask } from '../../api/todo.js'
import { initTodoTasks } from "../todo/reducers/todoSlice"

export const useTodos = () => {
  const dispatch = useDispatch()

  const loadTodos = async () => {
    const response = await getTodoTasks()
    dispatch(initTodoTasks(response.data))
  }

  const updateTodo = async (id, todoTask) => {
    await updateTodoTask(id, todoTask)
    await loadTodos()
  }

  const deleteTodo = async (id) => {
    await deleteTodoTask(id)
    await loadTodos()
  }

  const addTodo = async (data) => {
    await addTodoTask(data)
    await loadTodos()
  }

  return {
    loadTodos,
    updateTodo,
    deleteTodo,
    addTodo
  }
}