import request from '../utils/request.js'

export function getTodoTasks() {
  return request({
    url: '/todos',
    method: 'get'
  })
}

export function updateTodoTask(id, data) {
  return request({
    url: '/todos/' + id,
    method: 'put',
    data: data
  })
}

export function addTodoTask(data) {
  return request({
    url: '/todos/',
    method: 'post',
    data
  })
}

export function deleteTodoTask(id) {
  return request({
    url: '/todos/' + id,
    method: 'delete'
  })
}