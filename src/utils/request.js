import axios from "axios";

const service = axios.create({
  // baseURL: 'https://64c0dafcfa35860bae9f849b.mockapi.io/api/v1',
  // baseURL: 'http://127.0.0.1:4523/m1/3067148-0-default/',
  baseURL: 'http://localhost:8080/',
  timeout: 50000
})

export default service